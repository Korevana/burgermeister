import java.util.List;

/**
 * 
 * @author Robin Schmidt, Joshua Brandes
 *
 */
public class ButtonZutat extends Button{
	
	private Category category;
	private Burger currentBurger;
	private Zutat zutat;

	public ButtonZutat(Burger currentBurger,Zutat zutat,List<GUIObject> GUIobjects,String text, Vector2 pos,Vector2 size, int frameWidth,float ratio,Category category) {
		super(GUIobjects,text, pos,size, frameWidth,ratio,new State[]{State.build});
		this.currentBurger =currentBurger;
		this.zutat = zutat;
	}
	
	public void update(Burger currentBurger,Vector2 pos, Vector2 size, int frameWidth) {
		super.update(pos, size, frameWidth);
		this.currentBurger =currentBurger;
	}

	public void update(Burger currentBurger,int xpos, int ypos, int xsize, int ysize, int frameWidth) {
		super.update(xpos, ypos, xsize, ysize, frameWidth);
		this.currentBurger = currentBurger;
	}

	@Override
	public void action() {
		if(!this.visable)
			return;
		if (currentBurger != null && zutat != null&& visable){
			if (currentBurger.getBroetchen()!= null){
				currentBurger.addZutat(zutat);
			}else if (zutat instanceof Broetchen){
				currentBurger.setBroetchen((Broetchen)zutat);
			}
		}
		
	}
	
	public Category getCategory(){
		return category;
	}
}
