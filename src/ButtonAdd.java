import java.awt.Color;
import java.util.List;

/**
 * This Button ends the game
 * 
 * @author Robin Schmidt
 *
 */
public class ButtonAdd extends Button {

	BurgerPainter parent;
	int emptySlot = 0;

	public ButtonAdd(List<GUIObject> GUIobjects, Vector2 pos, Vector2 size, int frameWidth, BurgerPainter parent) {
		super(GUIobjects, "+", pos, size, frameWidth, 1, new State[] { State.build });
		this.parent = parent;
		super.fillColor = Color.YELLOW;
	}

	@Override
	public void action() {
		if (!this.visable)
			return;
		
		if (parent.burgers[9]!= null){
			System.out.println("10 Burger limit reached");
			return;	
		}
		if (parent.state == State.start) {
			parent.burgers[0] = new Burger();

		} else {
			do {
				emptySlot++;
			} while (parent.burgers[emptySlot] != null);
			parent.burgers[emptySlot] = new Burger();
		}
		parent.currentBurger = parent.burgers[emptySlot];
		parent.state = State.build;
		parent.category = Category.bread;
		return;
	}
}
