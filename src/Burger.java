import java.util.LinkedList;
import java.util.List;

/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public class Burger {

	private String burgername;
	private Broetchen broetchen;
	private List<Zutat> zutaten = new LinkedList<Zutat>();
	private final int maxZutaten = 8;
    private boolean klassisch = true;
    private boolean vegetarisch = true;
    private boolean vegan = true;
	
	/**
	 * Dynamische Gesamthoehen berechnung. 
	 * @return
	 */
	public int zubereiten(){
		for(int i = 0; i < zutaten.size(); i++){
			zutaten.get(i).zubereiten();
		}
		return getHoehe();
	}
		
	/**
	 * Methode zum Hinzufuegen von Zutaten.
	 * @param pos
	 * @param zutat
	 */
	public void addZutat(Zutat zutat){
		if(zutaten.size() <= maxZutaten ){
			zutaten.add(zutat);
			if(!zutat.isVegetarisch()){
				vegetarisch = false;
			}
			if(!zutat.isVegan()){
				vegan = false;
			}
			if(!zutat.isKlassisch()){
				klassisch = false;
			}
		}
		

	}

	/**
	 * Methode zum entfernen von Zutaten. 
	 * @param pos
	 */
	public void removeZutat(int pos){
		zutaten.remove(pos);

		klassisch = true;
		vegetarisch = true;
		vegan = true;
		for(int i = 0; i < zutaten.size(); i++){
			if(!zutaten.get(pos).isVegetarisch()){
				vegetarisch = false;
			}
			if(!zutaten.get(pos).isVegan()){
				vegan = false;
			}
			if(!zutaten.get(pos).isKlassisch()){
				klassisch = false;
			}
		}
	}
	
	public void removeAllZutat(){
		broetchen=null;
		zutaten.removeAll(zutaten);
		klassisch = true;
		vegetarisch = true;
		vegan = true;
	}
	
	public String getType(){
		if(klassisch){
			return "klassisch";
		}else if(vegan){
			return "vegan";
		}else if(vegetarisch){
			return "vegetarisch";
		}
		return "unbestimmt";
	}


	public List<Zutat> getZutaten(){
		return zutaten;
	}
	public Zutat[] getZutatenAsArray(){
		return (Zutat[])  zutaten.toArray();
	}
	
	public int getZeit(){
		int temp = 0;
		for(int i = 0; i < zutaten.size(); i++){
			temp = temp + zutaten.get(i).zubereiten();
		}
		temp += broetchen.zubereiten();
		return temp;
		}
	
	public int getHoehe(){
		int temp = 0;
		for(int i = 0; i < zutaten.size(); i++){
			temp = temp + zutaten.get(i).berechneHoehe();
		}
		temp += broetchen.berechneHoehe();
		return temp;
		}

	public float getPreis(){
		float temp = 0;
		for(int i = 0; i < zutaten.size(); i++){
			temp = temp + zutaten.get(i).getPreis();
		}
		temp += broetchen.getPreis();
		return temp;
		}

	public String getBurgername() {
		return burgername;
		}

	public Broetchen getBroetchen() {return broetchen;}

	public void setBroetchen(Broetchen broetchen) {
		this.broetchen = broetchen;
		}
}
