import java.awt.Color;
import java.util.List;

/**
 * This button is to switch between categorys
 * 
 * @author Robin Schmidt
 *
 */
public class ButtonState extends Button {

	private BurgerPainter parent;
	private State state;
	private List<GUIObject> GUIObjects;

	public ButtonState(List<GUIObject> GUIobjects, String text, Vector2 pos, Vector2 size, int frameWidth, State state,
			BurgerPainter parent) {
		super(GUIobjects, text, pos, size, frameWidth, 1, new State[] { State.build });
		this.parent = parent;
		this.state = state;
		this.GUIObjects = GUIobjects;
		updateColor(Color.YELLOW);
	}

	@Override
	public void action() {
		parent.state=state;
	}
}
