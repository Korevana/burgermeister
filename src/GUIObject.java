import java.awt.*;
import java.util.List;

/**
 * GUIObject is the main abstract for all GUI-Elements
 * @author Robin Schmidt
 *
 */
public abstract class GUIObject {
	
	Vector2 pos;
	Vector2 size;
	boolean visable;
	double ratio = 1f;
	State[] states;
	/**
	 * Constructor for a GUI Object
	 * @param pos
	 * @param size
	 */
	public GUIObject(List<GUIObject> GUIobjects,Vector2 pos, Vector2 size, double ratio, State[] state){
		GUIobjects.add(this);
		this.pos = new Vector2(pos.x-(size.x/2),pos.y-(size.y/2));
		this.size = new Vector2 ((int)(size.x*ratio),size.y);
		this.ratio = ratio;
		this.visable = true;
		this.states = state;
	}
	/**
	 * update Position with a Vector2 and Size with a Vector2.
	 * @param pos
	 * @param size
	 */
	public void update(Vector2 pos, Vector2 size){
		this.pos = new Vector2(pos.x-(size.x/2),pos.y-(size.y/2));
		this.size = new Vector2 ((int)(size.x*ratio),size.y);
	}
	
	/**
	 * update Position with a Vector2 and Size with two individual integer.
	 * @param pos
	 * @param sizeX
	 * @param sizeY
	 */
	public void update(Vector2 pos, int sizeX, int sizeY){
		update(pos,new Vector2((int)(sizeX*ratio),sizeY));
	}
	
	/**
	 * update Position with two individual integer  and Size with a Vector2
	 * @param posX
	 * @param posY
	 * @param size
	 */
	public void update(int posX,int posY, Vector2 size){
		update(new Vector2(posX,posY),size);
	}
	
	/**
	 * update Position and Size with four individual integer
	 * @param posX
	 * @param posY
	 * @param sizeX
	 * @param sizeY
	 */
	public void update(int posX,int posY, int sizeX,int sizeY){
		update(new Vector2(posX,posY),new Vector2(sizeX,sizeY));
	}
	
	/**
	 * update Position and Size with two individual integer
	 * @param pos
	 * @param size
	 */
	public void update(int pos, int size){
		update(new Vector2(pos,pos),new Vector2(size,size));
	}
	/**
	 * returns the Screen pixel Coords as an Int[4] array [0]xStart,[1]xEnd,[2]2Start,[3]yEnd
	 * @return
	 */
	public int[] getScreenCoords(){
		return new int[]{pos.x ,pos.x+size.x,pos.y,pos.y+size.y};
	}
	
	public void setVisable(){
		visable =true;
	}
	public void hide(){
		visable = false;
	}
	
	public abstract void paint(Graphics g);
}
