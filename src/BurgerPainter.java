import java.applet.Applet;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;

/**
 * With this awesome applet the user should be able to cusomize its bream Burger
 * 
 * @author Robin Schmidt, Joshua Brandes, Julian Bueche, Jens Emmel
 *
 */

@SuppressWarnings("serial")
public class BurgerPainter extends Applet implements MouseListener {

	Burger[] burgers = new Burger[] { new Burger(), null, null, null, null, null, null, null, null, null };
	// Collect images
	Image imgBackground, imgShadow;
	Image imgBreadBottomStd, imgBreadTopSesam, imgBreadTopStd, imgBreadBottomVegan, imgBreadTopVegan,
			imgBreadBottomCiabatta, imgBreadTopCiabatta;
	Image imgBeef, imgChicken, imgVegi, imgFalaffel, imgSalat, imgRucola, imgTomato, imgGherkin, imgOnion, imgJalapeno,
			imgKetchup, imgSandwich, imgChilli, imgHoney;
	Image imgGauda,imgSWCheese,imgBerg;
	// Setup Container
	Container application, leftHalf, rightHalf, bigBurger;
	Container[] burgerContainer = new Container[] { 
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE), 
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE), 
			new Container(Vector2.ONE, Vector2.ONE),
			new Container(Vector2.ONE, Vector2.ONE) 
			};
	Zutat currentZutat;
	Burger currentBurger;
	Category category = Category.bread;
	State state = State.start;
	List<GUIObject> GUIobjects = new LinkedList<GUIObject>();
	
	Button[] buttonBurger = new ButtonBurger[]{
			 new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,0,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,1,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,2,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,3,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,4,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,5,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,6,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,7,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,8,this)
			,new ButtonBurger(GUIobjects,"",Vector2.ONE,Vector2.ONE,0,9,this)
			};
	
	public Button[] getButtonBurger(){
		return buttonBurger;
	}
	// Create Zutaten
	Zutat breadStd = new Broetchen(90, 27, 10, "Standard Broetchen", 0.85f, true, false, true);
	Zutat breadSesam = new Broetchen(90, 28, 11, "Seasam Broetchen", 0.95f, false, false, true);
	Zutat breadVegan = new Broetchen(240, 34, 12, "Vegan-Broetchen", 0.55f, false, true, true);
	Zutat breadCiabatta = new Broetchen(330, 41, 13, "Ciabatta", 0.45f, false, false, true);
	Zutat beef = new Bratling(270, 25, 20, "Rindfleisch", 1.85f, true, false, false);
	Zutat chicken = new Bratling(180, 11, 21, "H�hnchenfleisch", 1.15f, false, false, false);
	Zutat falaffel = new Bratling(210, 21, 22, "Falaffel-Bratling", 1.45f, false, true, true);
	Zutat vegi = new Bratling(240, 25, 23, "Gemuese-Bratling", 1.75f, false, true, true);
	Zutat salat = new Salat(30, "Eisbergsalat", 0.18f, true, true, true);
	Zutat ruccola = new Salat(31, "Rucolasalat", 0.25f, false, true, true);
	Zutat tomato = new Gemuese(3, 3, 40, "Tomate", 0.25f, true, true, true);
	Zutat gherkin = new Gemuese(2, 4, 41, "Salzgurke", 0.15f, true, true, true);
	Zutat onion = new Gemuese(2, 5, 42, "Rote Zwiebelringe", 0.08f, false, true, true);
	Zutat jalapeno = new Gemuese(2, 5, 43, "Jalapeno-Ringe", 0.08f, false, true, true);
	Zutat sKetchup = new Sauce(5, "normal", 50, "Ketchup", 0.10f, true, true, true);
	Zutat sSandwich = new Sauce(10, "normal", 51, "Sandwich-Sauce", 0.15f, true, false, true);
	Zutat sChilli = new Sauce(8, "scharf", 52, "Chilli-Sauce", 0.25f, false, true, true);
	Zutat sHoney = new Sauce(8, "sue�", 53, "Ketchup", 0.18f, false, false, true);
	Zutat cGauda = new Cheese(55, "Gauda", 0.18f, false, false, false);
	Zutat cSWCheese = new Cheese( 56, "Sandwitch", 0.18f, true, false, false);
	Zutat cBerg = new Cheese(57, "Bergkaese", 0.18f, false, false, false);

	// Create GUIObjects after the initialization of the list
	Button bQuit = new ButtonQuit(GUIobjects, "Quit", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
	Button sEdit = new ButtonState(GUIobjects, "Edit", Vector2.ZERO, Vector2.ZERO, 0, State.build, this);
	Button sCollection = new ButtonState(GUIobjects, "|||", Vector2.ZERO, Vector2.ZERO, 0, State.collection, this);
	Button sDetails = new ButtonState(GUIobjects, "Details", Vector2.ZERO, Vector2.ZERO, 0, State.details, this);
	Button sBestellen = new ButtonState(GUIobjects, "Order", Vector2.ZERO, Vector2.ZERO, 0, State.bestellen, this);
	Button sReturn = new ButtonState(GUIobjects, "Back", Vector2.ZERO, Vector2.ZERO, 0, State.reset, this);
	Button bAdd = new ButtonAdd(GUIobjects, Vector2.ZERO, Vector2.ZERO, 0, this);
	Button cBread = new ButtonCategory(GUIobjects, "B", Vector2.ZERO, Vector2.ZERO, 0, Category.bread, this);
	Button cSalat = new ButtonCategory(GUIobjects, "S", Vector2.ZERO, Vector2.ZERO, 0, Category.salat, this);
	Button cMeat = new ButtonCategory(GUIobjects, "F", Vector2.ZERO, Vector2.ZERO, 0, Category.beef, this);
	Button cSouce = new ButtonCategory(GUIobjects, "S", Vector2.ZERO, Vector2.ZERO, 0, Category.souce, this);
	Button cCheese = new ButtonCategory(GUIobjects, "C", Vector2.ZERO, Vector2.ZERO, 0, Category.cheese, this);
	Button cAdd = new ButtonCategory(GUIobjects, "G", Vector2.ZERO, Vector2.ZERO, 0, Category.adds, this);
	
	

	// Button broetchen = new ButtonZutat(GUIobjects, "Broetchen", new
	// Vector2(100, 100), new Vector2(100,100), 10, (16f / 9f));
	ButtonZutat bStandart = new ButtonZutat(currentBurger, breadStd, GUIobjects, "Standard", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.bread);
	ButtonZutat bSesam = new ButtonZutat(currentBurger, breadSesam, GUIobjects, "Sesam", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.bread);
	ButtonZutat bVegan = new ButtonZutat(currentBurger, breadVegan, GUIobjects, "Vegan", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.bread);
	ButtonZutat bCiabatta = new ButtonZutat(currentBurger, breadCiabatta, GUIobjects, "Ciabatta", Vector2.ZERO,
			Vector2.ZERO, 0, (16f / 9f), Category.bread);
	ButtonZutat bBeef = new ButtonZutat(currentBurger, beef, GUIobjects, "Rind", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.beef);
	ButtonZutat bChicken = new ButtonZutat(currentBurger, chicken, GUIobjects, "Haenchen", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.beef);
	ButtonZutat bFalaffel = new ButtonZutat(currentBurger, falaffel, GUIobjects, "Falafel", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.beef);
	ButtonZutat bVegi = new ButtonZutat(currentBurger, vegi, GUIobjects, "Gemuese", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.beef);
	ButtonZutat bRucola = new ButtonZutat(currentBurger, ruccola, GUIobjects, "Rucola", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.salat);
	ButtonZutat bSalat = new ButtonZutat(currentBurger, salat, GUIobjects, "Eisberg", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.salat);
	ButtonZutat bTomato = new ButtonZutat(currentBurger, tomato, GUIobjects, "Tomate", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.adds);
	ButtonZutat bGherkin = new ButtonZutat(currentBurger, gherkin, GUIobjects, "Salzgurke", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.adds);
	ButtonZutat bOnions = new ButtonZutat(currentBurger, onion, GUIobjects, "Zwiebel", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.adds);
	ButtonZutat bJalapeno = new ButtonZutat(currentBurger, jalapeno, GUIobjects, "Jalapeno", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.adds);
	ButtonZutat bKetchup = new ButtonZutat(currentBurger, sKetchup, GUIobjects, "Ketchup", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.souce);
	ButtonZutat bSandwich = new ButtonZutat(currentBurger, sSandwich, GUIobjects, "Sandwich", Vector2.ZERO,
			Vector2.ZERO, 0, (16f / 9f), Category.souce);
	ButtonZutat bChilli = new ButtonZutat(currentBurger, sHoney, GUIobjects, "HonigSenf", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.souce);
	ButtonZutat bHoney = new ButtonZutat(currentBurger, sChilli, GUIobjects, "Chili", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.souce);
	ButtonZutat bGauda = new ButtonZutat(currentBurger, cGauda, GUIobjects, "Gauda", Vector2.ZERO, Vector2.ZERO,
			0, (16f / 9f), Category.souce);
	ButtonZutat bSWCheese = new ButtonZutat(currentBurger, cSWCheese, GUIobjects, "Sandwitch", Vector2.ZERO,
			Vector2.ZERO, 0, (16f / 9f), Category.souce);
	ButtonZutat bBerg = new ButtonZutat(currentBurger, cBerg, GUIobjects, "Bergkaese", Vector2.ZERO, Vector2.ZERO, 0,
			(16f / 9f), Category.souce);


	public void init() {
		this.setSize(1920, 1080);
		this.addMouseListener(this);

		imgBackground = getImage(getClass().getResource("/Background_Gradient.jpg"));
		imgShadow = getImage(getClass().getResource("BurgerShadow.png"));

		// create Zutatenliste!
		initZutaten();

		// Set a defaultBurger for startMenue
		burgers[0].setBroetchen((Broetchen) breadSesam);
		burgers[0].addZutat(beef);
		burgers[0].addZutat(salat);
		burgers[0].addZutat(ruccola);
		burgers[0].addZutat(tomato);
		System.out.println(burgers[0].getHoehe());

		application = new Container(Vector2.ZERO, this.getWidth(), this.getHeight());
		leftHalf = new Container(Vector2.ZERO, application.getPercentageX(50), application.getPercentageY(75));
		rightHalf = new Container(application.getPercentageX(50), 0, application.getPercentageX(50),
				application.getPercentageY(75));
		bigBurger = new Container(rightHalf.pos, application.getPercentage(50));
		currentBurger = burgers[0];
	}

	public void paint(Graphics g) {
		update();
		g.drawImage(imgBackground, 0, 0, application.getPercentageX(100), application.getPercentageY(100), this);
		for (int i = 0; i < GUIobjects.size(); i++) {
			if (GUIobjects.get(i).visable)
				GUIobjects.get(i).paint(g);
		}

		if (currentBurger != null && currentBurger.getBroetchen() != null)
			displayBurger(bigBurger, currentBurger, g);
		if (state == State.collection) {
			for (int i = 0; i < buttonBurger.length; i++) {
				
				
			}
			for (int i = 0; i < burgers.length; i++) {
				if (burgers[i] != null){
					buttonBurger[i].paint(g);
					displayBurger(burgerContainer[i], burgers[i], g);
				}
			}
		}
	}

	/**
	 * Updates GUI Positioning
	 */
	public void update() {
		// update Container
		application.updateContainer(Vector2.ZERO, this.getWidth(), this.getHeight());
		leftHalf.updateContainer(Vector2.ZERO, application.getPercentageX(50), application.getPercentageY(90));
		rightHalf.updateContainer(application.getPercentageX(50), 0, application.getPercentageX(50),
				application.getPercentageY(90));
		bigBurger.updateContainer(Vector2.ZERO, leftHalf.getPercentageX(100), leftHalf.getPercentageY(75));
		//updateBurgerContainer();
		// update Buttons
		bQuit.update(application.getPercentage(90), application.getPercentageXSquare(7), 5);

		// Hide all Buttons
		for (int i = 0; i < GUIobjects.size(); i++) {
			GUIobjects.get(i).hide();
		}
		// Unhide Mainmenue
		bQuit.setVisable();
		bAdd.setVisable();

		if (state == State.start) {
			bAdd.update(rightHalf.getPercentageX(50), rightHalf.getPercentageY(55), application.getPercentageXSquare(7),5);

		} else if (state == State.collection) {
			sEdit.setVisable();
			sDetails.setVisable();
			sBestellen.setVisable();
			bAdd.update(leftHalf.getPercentageX(10), leftHalf.getPercentageY(100), application.getPercentageX(7), application.getPercentageY(7), 5);
			sEdit.update(leftHalf.getPercentageX(24), leftHalf.getPercentageY(100),application.getPercentageX(7), application.getPercentageY(7), 5);
			sDetails.update(leftHalf.getPercentageX(39), leftHalf.getPercentageY(100),application.getPercentageX(9), application.getPercentageY(7), 5);
			sBestellen.update(leftHalf.getPercentageX(54), leftHalf.getPercentageY(100), application.getPercentageX(7), application.getPercentageY(7), 5);
			updateBurgerContainer();

		}else if(state == State.bestellen){
			bAdd.hide();
			int gesHoehe = 0;
			int gesDauer = 0;
			float gesPreis = 0;
			for(int i = 0; i < burgers.length; i++){
				if(burgers[i] != null&&burgers[i].getBroetchen()!= null){
					gesHoehe += burgers[i].getHoehe();
					gesDauer += burgers[i].getZeit();
					gesPreis += burgers[i].getPreis();
				}
			}
			Button tBestellung0 = new ButtonText(GUIobjects, "Gesamte Hoehe: " + gesHoehe + "mm", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			Button tBestellung1 = new ButtonText(GUIobjects, "Gesamte Zubereitungszeit: " + gesDauer / 60 + "min", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			Button tBestellung2 = new ButtonText(GUIobjects, "Gesamter Preis: " + gesPreis + "EUR", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			tBestellung0.update(application.getPercentageX(40), application.getPercentageY(10), application.getPercentageX(30), application.getPercentageY(7), 5);
			tBestellung1.update(application.getPercentageX(40), application.getPercentageY(40), application.getPercentageX(30), application.getPercentageY(7), 5);
			tBestellung2.update(application.getPercentageX(40), application.getPercentageY(70), application.getPercentageX(30), application.getPercentageY(7), 5);
			System.out.println(gesHoehe + " " + gesDauer + " " + gesPreis);
		}else if(state == State.details){
			sReturn.setVisable();
			bAdd.hide();
			int hoehe = 0;
			int dauer = 0;
			float preis = 0;
			String typ = "";
			for(int i = 0; i < buttonBurger.length; i++){
				if(((ButtonBurger) buttonBurger[i]).getPressed()){
					hoehe = burgers[i].getHoehe();
					dauer = burgers[i].getZeit();
					preis = burgers[i].getPreis();
					typ = burgers[i].getType();
				}
			}
			Button tDetails0 = new ButtonText(GUIobjects, "Hoehe: " + hoehe + "mm", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			Button tDetails1 = new ButtonText(GUIobjects, "Zubereitungszeit: " + dauer / 60 + "min", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			Button tDetails2 = new ButtonText(GUIobjects, "Preis: " + preis + "EUR", Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			Button tDetails3 = new ButtonText(GUIobjects, "Typ: " + typ, Vector2.ZERO, Vector2.ZERO, 0, (16f / 9f));
			tDetails0.update(application.getPercentageX(60), application.getPercentageY(10), application.getPercentageX(20), application.getPercentageY(7), 5);
			tDetails1.update(application.getPercentageX(60), application.getPercentageY(30), application.getPercentageX(20), application.getPercentageY(7), 5);
			tDetails2.update(application.getPercentageX(60), application.getPercentageY(50), application.getPercentageX(20), application.getPercentageY(7), 5);
			tDetails3.update(application.getPercentageX(60), application.getPercentageY(70), application.getPercentageX(20), application.getPercentageY(7), 5);
			sReturn.update(leftHalf.getPercentageX(10), leftHalf.getPercentageY(100), application.getPercentageX(7), application.getPercentageY(7), 5);
			System.out.println(hoehe + " " + dauer + " " + preis);
		}else if(state == State.reset){
			for(int i = 0; i < buttonBurger.length; i++){
				((ButtonBurger) buttonBurger[i]).setPressed(false);
				buttonBurger[i].updateColor(Color.WHITE);
			}
			state = State.collection;
			update();
		}else if (state == State.build) {
			sCollection.setVisable();
			sCollection.update(leftHalf.getPercentageX(24), leftHalf.getPercentageY(100), application.getPercentageX(7),
					application.getPercentageX(7), 5);
			bAdd.update(leftHalf.getPercentageX(10), leftHalf.getPercentageY(100), application.getPercentageX(7),
					application.getPercentageX(7), 5);

			cBread.setVisable();
			cMeat.setVisable();
			cSalat.setVisable();
			cAdd.setVisable();
			cSouce.setVisable();
			cCheese.setVisable();
			// Categorys
			cBread.update(rightHalf.getPercentageX(100) - application.getPercentageX(4), rightHalf.getPercentageY(10),
					application.getPercentageXSquare(7), 5);
			cMeat.update(rightHalf.getPercentageX(100) - application.getPercentageX(4), rightHalf.getPercentageY(25),
					application.getPercentageXSquare(7), 5);
			cSalat.update(rightHalf.getPercentageX(100) - application.getPercentageX(4), rightHalf.getPercentageY(40),
					application.getPercentageXSquare(7), 5);
			cAdd.update(rightHalf.getPercentageX(100) - application.getPercentageX(4), rightHalf.getPercentageY(55),
					application.getPercentageXSquare(7), 5);
			cSouce.update(rightHalf.getPercentageX(100) - application.getPercentageX(4), rightHalf.getPercentageY(70),
					application.getPercentageXSquare(7), 5);
			cCheese.update(rightHalf.getPercentageX(100) - application.getPercentageX(4), rightHalf.getPercentageY(85),
					application.getPercentageXSquare(7), 5);

			// update ZutatenButtons and check category

			if (category == Category.bread) {
				bStandart.setVisable();
				bSesam.setVisable();
				bVegan.setVisable();
				bCiabatta.setVisable();
			} else if (category == Category.beef) {
				bBeef.setVisable();
				bChicken.setVisable();
				bFalaffel.setVisable();
				bVegi.setVisable();
			} else if (category == Category.salat) {
				bSalat.setVisable();
				bRucola.setVisable();
			} else if (category == Category.adds) {
				bGherkin.setVisable();
				bTomato.setVisable();
				bOnions.setVisable();
				bJalapeno.setVisable();
			} else if (category == Category.souce) {
				bKetchup.setVisable();
				bSandwich.setVisable();
				bChilli.setVisable();
				bHoney.setVisable();
			} else if (category == Category.cheese){
				bGauda.setVisable();
				bSWCheese.setVisable();
				bBerg.setVisable();
			}
			Vector2 buttonSize =new Vector2(application.getPercentageX(10),application.getPercentageX(5));
			bStandart.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(25),buttonSize.x,buttonSize.y, 5);
			bSesam.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(40),buttonSize.x,buttonSize.y, 5);
			bVegan.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(55), buttonSize.x,buttonSize.y, 5);
			bCiabatta.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(70),buttonSize.x,buttonSize.y, 5);
			bBeef.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(25),buttonSize.x,buttonSize.y, 5);
			bChicken.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(40),buttonSize.x,buttonSize.y, 5);
			bFalaffel.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(55),buttonSize.x,buttonSize.y, 5);
			bVegi.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(70),buttonSize.x,buttonSize.y, 5);
			bSalat.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(40),buttonSize.x,buttonSize.y, 5);
			bRucola.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(55),buttonSize.x,buttonSize.y, 5);
			bGherkin.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(40),buttonSize.x,buttonSize.y, 5);
			bTomato.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(55),buttonSize.x,buttonSize.y, 5);
			bOnions.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(70),buttonSize.x,buttonSize.y, 5);
			bKetchup.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(85),buttonSize.x,buttonSize.y, 5);
			bSandwich.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(40),buttonSize.x,buttonSize.y, 5);
			bHoney.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(55),buttonSize.x,buttonSize.y, 5);
			bChilli.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(70),buttonSize.x,buttonSize.y, 5);
			bJalapeno.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(85),buttonSize.x,buttonSize.y, 5);
			bGauda.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(70),buttonSize.x,buttonSize.y, 5);
			bSWCheese.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(40),buttonSize.x,buttonSize.y, 5);
			bBerg.update(currentBurger, rightHalf.getPercentageX(20), rightHalf.getPercentageY(55),buttonSize.x,buttonSize.y, 5);
			
		}
	}

	/**
	 * Updates the Burger Container
	 */
	private void updateBurgerContainer() {
		for (int i = 0; i < burgerContainer.length; i++) {
			Vector2 tempPos = new Vector2(rightHalf.getPercentageX(25), rightHalf.getPercentageY(10));
			if (i < 5) {
				tempPos.y = rightHalf.getPercentageY((i * 20) - 10);
			} else {
				tempPos.y = rightHalf.getPercentageY(-110 + (i * 20));
				tempPos.x = rightHalf.getPercentageX(50);
			}
			burgerContainer[i].updateContainer(tempPos, application.getPercentageXSquare(12));
			if (burgers[i] != null){
			buttonBurger[i].setVisable();
			buttonBurger[i].update(tempPos.x+application.getPercentageX(6), tempPos.y+application.getPercentageX(12),application.getPercentageXSquare(6));
			}
		}
	}

	/**
	 * Helper methode to Paint an Image keep the code clean.
	 * 
	 * @param image
	 * @param position
	 * @param size
	 * @param middleSize
	 * @param burgerSize
	 * @param g
	 */
	private void drawZutatImage(Image image, Container position, double size, int middleSize, int burgerSize,
			Graphics g) {
		if (image == null)
			return;
		if(state != State.bestellen){
			g.drawImage(image, position.getPercentageX(50) - middleSize, position.getPercentageY(100) - burgerSize,
					(int) (image.getWidth(this) * size), (int) (image.getHeight(this) * size), this);
		}	
	}

	/**
	 * this Methode visualize a Burger centered in a given Container
	 * 
	 * @param position
	 * @param burger
	 * @param g
	 */
	private void displayBurger(Container position, Burger burger, Graphics g) {
		if (burger == null || burger.getBroetchen() == null)
			return;

		double size = (float) (position.size.x) / 1500;
		int middleSize = (int) (800 * size) / 2;
		Broetchen bread = burger.getBroetchen();
		List<Zutat> zutaten = burger.getZutaten();

		// Paint Shadow, Base
		if (burger.getBroetchen() != null)
			drawZutatImage(imgShadow, position, size, middleSize, 0, g);

		int burgerSize = (int) (35 * size);

		// Paint BottomBread
		drawZutatImage(zutatToImage(bread.getNummer()), position, size, middleSize, burgerSize, g);
		burgerSize += (int) (bread.berechneHoehe() * 4 * size);

		for (int i = 0; i < zutaten.size(); i++) {
			Zutat tempZutat = zutaten.get(i);
			Image imageToDraw = zutatToImage(tempZutat.getNummer());
			drawZutatImage(imageToDraw, position, size, middleSize, burgerSize, g);
			burgerSize += (int) Math.max((tempZutat.berechneHoehe() * 5), 20) * size;
		}
		burgerSize += (int) (bread.berechneHoehe() * 2 * size);
		drawZutatImage(zutatToImage(burger.getBroetchen().getNummer() - 10), position, size, middleSize, burgerSize, g);
	}

	/**
	 * Collect the image that suits the Zutat.
	 * 
	 * @param number
	 *            of the Zutat.
	 * @return The related Image.
	 */
	private Image zutatToImage(int number) {
		switch (number) {
		// Bread
		case 0:
			return imgBreadTopStd;
		case 1:
			return imgBreadTopSesam;
		case 2:
			return imgBreadTopVegan;
		case 3:
			return imgBreadTopCiabatta;
		case 10:
			return imgBreadBottomStd;
		case 11:
			return imgBreadBottomStd;
		case 12:
			return imgBreadBottomVegan;
		case 13:
			return imgBreadBottomCiabatta;
		// Bratlinge
		case 20:
			return imgBeef;
		case 21:
			return imgChicken;
		case 22:
			return imgFalaffel;
		case 23:
			return imgVegi;
		// Salat
		case 30:
			return imgSalat;
		case 31:
			return imgRucola;
		case 40:
			return imgTomato;
		case 41:
			return imgGherkin;
		case 42:
			return imgOnion;
		case 43:
			return imgJalapeno;
		case 50:
			return imgKetchup;
		case 51:
			return imgSandwich;
		case 52:
			return imgChilli;
		case 53:
			return imgHoney;
		case 55:
			return imgGauda;
		case 56:
			return imgSWCheese;
		case 57:
			return imgBerg;
		default:
		}
		return imgVegi;
	}

	/**
	 * Creates all Zutaten
	 */
	private void initZutaten() {
		/*
		 * Create Zutaten Object Create Image Object
		 */
		imgBreadBottomStd = getImage(getClass().getResource("BurgerStdBottom.png"));
		imgBreadTopStd = getImage(getClass().getResource("BurgerStdTop.png"));
		imgBreadBottomStd = getImage(getClass().getResource("BurgerStdBottom.png"));
		imgBreadTopSesam = getImage(getClass().getResource("BurgerSesamTop.png"));
		imgBreadBottomVegan = getImage(getClass().getResource("BreadBottomVegan.png"));
		imgBreadTopVegan = getImage(getClass().getResource("BreadTopVegan.png"));
		imgBreadBottomCiabatta = getImage(getClass().getResource("BreadBottomCiabatta.png"));
		imgBreadTopCiabatta = getImage(getClass().getResource("BreadTopCiabatta.png"));
		imgBeef = getImage(getClass().getResource("Beef.png"));
		imgChicken = getImage(getClass().getResource("Chicken.png"));
		imgFalaffel = getImage(getClass().getResource("Falaffel.png"));
		imgVegi = getImage(getClass().getResource("Vegy.png"));
		imgSalat = getImage(getClass().getResource("EisSalat.png"));
		imgRucola = getImage(getClass().getResource("Rucola.png"));
		imgTomato = getImage(getClass().getResource("Tomato.png"));
		imgGherkin = getImage(getClass().getResource("Gherkin.png"));
		imgOnion = getImage(getClass().getResource("Onions.png"));
		imgJalapeno = getImage(getClass().getResource("Jalapenos.png"));
		imgKetchup = getImage(getClass().getResource("Ketchup.png"));
		imgSandwich = getImage(getClass().getResource("SandwichSauce.png"));
		imgChilli = getImage(getClass().getResource("ChiliSauce.png"));
		imgHoney = getImage(getClass().getResource("HoneyMustard.png"));
		imgGauda =getImage(getClass().getResource("HoneyMustard.png"));
		imgSWCheese =getImage(getClass().getResource("HoneyMustard.png"));
		imgBerg =getImage(getClass().getResource("HoneyMustard.png"));
	}

	// Mouse Stuff
	@Override
	public void mouseClicked(MouseEvent e) {
		int x, y;
		x = e.getX(); // x-Koordinate, an der Mausereignis stattgefunden hat
		y = e.getY(); // y-Koordinate, an der Mausereignis stattgefunden hat
		System.out.println("Mouse pos X: " + x + " Y: " + y);
		if (GUIobjects.size() <= 0)
			return;

		for (int i = 0; i < GUIobjects.size(); i++) {
			if (GUIobjects.get(i) instanceof Button&&GUIobjects.get(i).visable) {
				Button tempButton = (Button) (GUIobjects.get(i));
				int[] screenCords = tempButton.getScreenCoords();

				// ueberprueft auf welchem man die Maus gedr�ckt h�lt
				if (y > screenCords[2] && y < screenCords[3] && x > screenCords[0] && x < screenCords[1]) {
					System.out.println(
							screenCords[0] + " " + screenCords[1] + " " + screenCords[2] + " " + screenCords[3]);
					System.out.println(
							"GUI Elements: " + GUIobjects.size() + " Type: " + GUIobjects.get(i).getClass().toString());

					tempButton.action();
				}
			}
		}
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
