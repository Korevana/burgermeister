import java.awt.Color;
import java.util.List;
/**
 * This button is to switch between categorys
 * @author Robin Schmidt
 *
 */
public class ButtonCategory extends Button {

	private boolean active = false;
	private BurgerPainter parent;
	private Category category;
	private List<GUIObject> GUIObjects;

	public ButtonCategory(List<GUIObject> GUIobjects, String text, Vector2 pos, Vector2 size, int frameWidth,
			Category category, BurgerPainter parent) {
		super(GUIobjects, text, pos, size, frameWidth, 1,new State[]{State.build});
		this.parent = parent;
		this.category = category;
		this.GUIObjects = GUIobjects;
	}

	@Override
	public void action() {
		if (!this.visable)
			return;
		if (!active) {
			parent.category = category;
			updateColor(Color.YELLOW);
			for (int i = 0; i < GUIObjects.size(); i++) {

				if (GUIObjects.get(i) instanceof ButtonCategory) {
					ButtonCategory categoryButton = (ButtonCategory) (GUIObjects.get(i));
					categoryButton.setInactive();
				}
			}
			active = true;
		}
	}

	public void update(Vector2 pos, Vector2 size, int frameWidth) {
		super.update(pos, size, frameWidth);
		if (!active)
			updateColor(Color.LIGHT_GRAY);
	}

	public void update(int xpos, int ypos, int xsize, int ysize, int frameWidth) {
		super.update(xpos, ypos, xsize, ysize, frameWidth);
		if (!active)
			updateColor(Color.LIGHT_GRAY);
	}

	public void setInactive() {
		active = false;
	}

}
