
/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public class Gemuese extends Zutat{

	private int scheibenDicke;
	private int scheibenAnzahl;
	
	/**
	 * Konstruktor. 
	 * @param scheibenDicke
	 * @param scheibenAnzahl
	 * @param nummer
	 * @param name
	 * @param preis
	 * @param klassisch
	 * @param vegan
	 * @param vegetarisch
	 */
	public Gemuese(int scheibenDicke, int scheibenAnzahl, int nummer, String name, float preis, boolean klassisch, boolean vegan, boolean vegetarisch){
		super(nummer, name, preis, klassisch, vegan, vegetarisch);
		this.scheibenDicke = scheibenDicke;
		this.scheibenAnzahl = scheibenAnzahl;
	}
	/**
	 * Ausgage das Gemuese geschnitten und zubereitet wird. 
	 */
	public int zubereiten(){
		System.out.println(name + " wird gewaschen.");
		System.out.println(name + " wird in " + scheibenAnzahl + " geschnitten und wird " + scheibenDicke + "dick geschnitten.");
		return scheibenAnzahl;
	}
	
	public int berechneHoehe(){
		return scheibenDicke;
		}
	
	public int getScheibenDicke(){
		return scheibenDicke;
	}
	
	public int getScheibenAnzahl(){
		return scheibenAnzahl;
	}
}
