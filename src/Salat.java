
/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public class Salat extends Zutat{

	
	/**
	 * Konstruktor.
	 * @param nummer
	 * @param name
	 * @param preis
	 * @param klassisch
	 * @param vegan
	 * @param vegetarisch
	 */
	public Salat(int nummer, String name, float preis, boolean klassisch, boolean vegan, boolean vegetarisch){
		super(nummer, name, preis, klassisch, vegan, vegetarisch);
	}
	
	/**
	 * Ausgabe fuer den Nutzer das der Salat gewaschen wird. 
	 */
	public int zubereiten(){
		System.out.println("Der " + name + " wird gewaschen");
		return 0;
	}
	
}
