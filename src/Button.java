import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;
/**
 * This is the Button Parent Class
 * @author Robin Schmidt
 *
 */
public abstract class Button extends GUIObject{

	protected int frameWidth;
	String text;
	Font font = new Font("Arial",Font.PLAIN,28);
	protected Color fillColor = new Color(250,250,255,255);
	protected Color frameColor = Color.GRAY;
	
	public Button(List<GUIObject> GUIobjects,String text,Vector2 pos, Vector2 size, int frameWidth,float ratio, State[] state){
		super(GUIobjects,pos,size,ratio,state);
		this.text = text;
		this.frameWidth = frameWidth;
	}
	
	public Button(List<GUIObject> GUIobjects,String text,Vector2 pos, Vector2 size, int frameWidth, State[] state){
		this(GUIobjects,text,pos,size,frameWidth,1f,state);
	}
	
	public void update(Vector2 pos,Vector2 size,int frameWidth){
		super.update(pos, size);
		this.frameWidth = frameWidth;
	}
	
	public void update(int xpos, int ypos, int xsize, int ysize, int frameWidth){
		super.update(xpos, ypos, xsize, ysize);
		this.frameWidth = frameWidth;
	}
	
	public void update(int xpos, int ypos, Vector2 size, int frameWidth){
		super.update(xpos, ypos,size);
		this.frameWidth = frameWidth;
	}

	public void update(Vector2 pos, int xsize, int ysize, int frameWidth){
		super.update(pos, xsize, ysize);
		this.frameWidth = frameWidth;
	}
	
	public void updateColor(Color color){
		this.fillColor = color;
	}
	
	public void paint(Graphics g){
		if (visable){
			int totalSizeX = size.x +(2*frameWidth);
			int totalSizeY = size.y +(2*frameWidth);
			g.setColor(frameColor);
			g.fillRect(pos.x-frameWidth, pos.y-frameWidth, totalSizeX, totalSizeY);
			g.setColor(fillColor);
			g.fillRect(pos.x, pos.y, size.x, size.y);
			g.setColor(Color.BLACK);
			font =new Font("Calibri",0, (size.y/4)*3);
			g.setFont(font);
			g.drawString(text, pos.x+10, pos.y+((size.y/4)*3));
		}
	}
	public abstract void action();
	
	protected void hover(){
		updateColor(Color.yellow);		
	}

}
