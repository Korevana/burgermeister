/**
 * 
 * @author Robin Schmidt
 *
 */
public enum State {start(0),build(1),details(2),collection(3), bestellen(4), reset(5);
	public int State;
	
	private State(int state){
		this.State=state;
	}
}
