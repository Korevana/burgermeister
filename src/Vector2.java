/**
 * The Vector 2 Class contains an x and y Data
 * @author Robin Schmidt
 *
 */
public class Vector2 {
	int x;
	int y;
	final static Vector2 ZERO = new Vector2(0,0);
	final static Vector2 ONE = new Vector2(1,1);
	
	public Vector2(int x,int y){
		this.x = x;
		this.y = y;
	}
	/**
	 * flips the position of x and y
	 */
	public void flip(){
		int temp = x;
		x=y;
		y=temp;
	}
}
