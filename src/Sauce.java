
/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public class Sauce extends Zutat{

	private int menge;
	private String geschmack;
	
	/**
	 * Konstruktor.
	 * @param menge
	 * @param geschmack
	 * @param nummer
	 * @param name
	 * @param preis
	 * @param klassisch
	 * @param vegan
	 * @param vegetarisch
	 */
	public Sauce(int menge, String geschmack, int nummer, String name, float preis, boolean klassisch, boolean vegan, boolean vegetarisch){
		super(nummer, name, preis, klassisch, vegan, vegetarisch);
		this.menge = menge;
		this.geschmack = geschmack;
	}
	
	/**
	 * Ausgabe das die Sauce geschuettelt wird. 
	 */
	public int zubereiten(){
		System.out.println(name + " wird geschuettelt.");
		return 0;
	}
	
	public int getMenge(){
		return menge;
	}
	
	public String getGeschmac(){
		return geschmack;
	}
}
