/**
 * This enum is to categorize Zutaten
 * @author Robin Schmidt
 *
 */
public enum Category {bread(0),beef(1),salat(2),souce(3),adds(4),cheese(5);
	
	int index;
	
	private Category(int index){
		this.index = index;
	}

}
