/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public class Bratling extends Zutat{

	private int bratzeit;
	private float hoehe;
	
	/**
	 * Konstruktor. 
	 * @param bratzeit
	 * @param hoehe
	 * @param nummer
	 * @param name
	 * @param preis
	 * @param klassisch
	 * @param vegan
	 * @param vegetarisch
	 */
	public Bratling (int bratzeit, int hoehe, int nummer, String name, float preis, boolean klassisch, boolean vegan, boolean vegetarisch){
		super(nummer, name, preis, klassisch, vegan, vegetarisch);
		this.bratzeit = bratzeit;
		this.hoehe = hoehe;
	}
	
	/**
	 * Ausgabe das gebraten wird. 
	 * dynamische hoehen berechnung. 
	 */
	public int zubereiten(){
		System.out.println("Das " + name + " wird " + bratzeit + " gebraten.");
		return bratzeit;
	}
	
	public int berechneHoehe(){
		return (int) (hoehe - ((hoehe / 100) * ((bratzeit / 60) * 3.5)));
	}
	
	public int getBackzeit(){
		return bratzeit;
	}

	public float getHoehe(){
		return hoehe;
	}
}
