
/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public abstract class Zutat {

	protected int nummer;
	protected String name;
	protected float preis;
	protected boolean klassisch;
	protected boolean vegan;
	protected boolean vegetarisch;
	
	public Zutat(int nummer, String name, float preis, boolean klassisch, boolean vegan, boolean vegetarisch){
		this.nummer = nummer;
		this.name = name;
		this.preis = preis;
		this.klassisch = klassisch;
		this.vegan = vegan;
		this.vegetarisch = vegetarisch;
	}

	
	public abstract int zubereiten();
	
	public int berechneHoehe(){
		return 0;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(nummer);
		sb.append(name);
		sb.append(preis);
		return sb.toString();
	}

	public int getNummer() {return nummer;}

	public String getName() {return name;}

	public float getPreis() {return preis;}

	public boolean isKlassisch() {return klassisch;}

	public boolean isVegan() {return vegan;}

	public boolean isVegetarisch() {return vegetarisch;}

}


