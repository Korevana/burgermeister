/**
 * Burgermeister-Projekt Joshua Brandes, Julian Bueche, Jens Emmel, Robin Schmidt
 * @author Julian Bueche, Jens Emmel
 */
public class Broetchen extends Zutat {

	private int backzeit;
	private int hoehe;
	
	/**
	 * Konstruktor.
	 * @param backzeit
	 * @param hoehe
	 * @param nummer
	 * @param name
	 * @param preis
	 * @param klassisch
	 * @param vegan
	 * @param vegetarisch
	 */
	public Broetchen (int backzeit, int hoehe, int nummer, String name, float preis, boolean klassisch, boolean vegan, boolean vegetarisch){
		super(nummer, name, preis, klassisch, vegan, vegetarisch);
		this.backzeit = backzeit;
		this.hoehe = hoehe;
		
	}
	
	/**
	 * Ausgabe fuer den Benuzter, dass das Broetchen gebacken wird. 
	 * Dynamische hoehenberechnung. 
	 */
	public int zubereiten(){
		System.out.println("Das " + name + " Broetchen wird " + backzeit + " gebacken ");
		return backzeit;
	}
	
	public int berechneHoehe(){		
		return (hoehe + ((hoehe / 100) * ((backzeit / 60) * 2)));
	}
	
	public int getBackzeit(){
		return backzeit;
	}
	
	public int getHoehe(){
		return hoehe;
	}
}
