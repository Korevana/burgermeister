import java.awt.Color;
import java.util.List;

/**
 * This button is to switch between categorys
 * 
 * @author Robin Schmidt
 *
 */
public class ButtonBurger extends Button {

	private BurgerPainter parent;
	private int burgerPosition;
	private List<GUIObject> GUIObjects;
	private boolean pressed;

	public ButtonBurger(List<GUIObject> GUIobjects, String text, Vector2 pos, Vector2 size, int frameWidth,
			int burgerPosition, BurgerPainter parent) {
		super(GUIobjects, text, pos, size, frameWidth, 1, new State[] { State.collection });
		this.parent = parent;
		this.burgerPosition = burgerPosition;
		this.GUIObjects = GUIobjects;
		this.pressed = false;
	}
	
	public boolean getPressed(){
		return pressed;
	}
	
	public void setPressed(boolean pressed2){
		pressed = pressed2;
	}

	@Override
	public void action() {
		if (!this.visable)
			return;
		parent.currentBurger = parent.burgers[burgerPosition];
		updateColor(Color.YELLOW);
		pressed = true;
	}
}
