/**
 * The Container class defines an percentage area were you can place Objects depending on their percentage
 * @author Robin Schmidt
 *
 */
	public class Container {
		Vector2 pos;
		Vector2 size;
		
		/**
		 * The Container Object is an align helper to work in percentage space.
		 * @param posX
		 * @param posY
		 * @param sizeX
		 * @param sizeY
		 */
		public Container(Vector2 pos, Vector2 size){
			this.pos = pos;
			this.size = size;
		}
		
		public Container(int posX,int posY,int sizeX,int sizeY){
			this(new Vector2(posX,posY),new Vector2(sizeX,sizeY));
		}
		
		public Container(int posX,int posY,Vector2 size){
			this(new Vector2(posX,posY),size);
		}
		
		public Container(Vector2 pos, int sizeX,int sizeY){
			this(pos,new Vector2(sizeX,sizeY));
		}
		
		public void updateContainer(Vector2 pos,Vector2 size){
			this.pos = pos;
			this.size = size;
		}
		
		public void updateContainer(int posX,int posY,int sizeX,int sizeY){
			updateContainer(new Vector2(posX,posY),new Vector2(sizeX,sizeY));
		}
		
		public void updateContainer(Vector2 pos,int sizeX,int sizeY){
			updateContainer(pos, new Vector2(sizeX,sizeY));
		}
		
		public int getPercentageX(int percentage){
			int percentageX =(int)((float)pos.x + (((float)size.x/100f)*percentage));
			return percentageX;
		}
		public int getPercentageY(int percentage){
			int percentageY =(int)((float)pos.y + ((float)(size.y/100f)*percentage));
			return percentageY;
		}
		public Vector2 getPercentage(int percentage){
			return new Vector2(getPercentageX(percentage),getPercentageY(percentage));
		}
		public Vector2 getPercentage(int percentageX, int percentageY){
			return new Vector2(getPercentageX(percentageX),getPercentageY(percentageY));
		}
		public Vector2 getPercentage(Vector2 percentageXY){
			return new Vector2(getPercentageX(percentageXY.x),getPercentageY(percentageXY.y));
		}
		public Vector2 getPercentageXSquare(int percentage){
			return new Vector2(getPercentageX(percentage),getPercentageX(percentage));
		}
}
