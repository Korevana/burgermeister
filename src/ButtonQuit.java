import java.util.List;
/**
 * This Button ends the game
 * @author Robin Schmidt
 *
 */
public class ButtonQuit extends Button {

	public ButtonQuit(List<GUIObject> GUIobjects, String text, Vector2 pos, Vector2 size, int frameWidth, float ratio) {
		super(GUIobjects, text, pos, size, frameWidth, ratio,new State[]{State.build});
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {
		if(!this.visable)
			return;
		System.exit(0);
		return;
	}
}
